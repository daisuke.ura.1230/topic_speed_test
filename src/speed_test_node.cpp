#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <iostream>
#include <string>

int main(int argc, char **argv) {
  ros::init(argc, argv, "speed_test");
  ros::NodeHandle node("~");

  int param;
  node.getParam("test", param);

  if (param == 0) {
    return 0;
  }

  ros::Publisher empty_pub = node.advertise<std_msgs::Empty>("/empty", 1);

  std::cout << "Rate: " << param << std::endl;

  ros::Rate rate(param);

  for(int i=5; i>0; i--)
  {
      std::cout << i << std::endl;
      ros::Rate(1.0).sleep();
  }

  for (int i = 0; i < param*10; i++) {
      empty_pub.publish(std_msgs::Empty());
      rate.sleep();
  }
  std::cout << "done!" << std::endl;
}