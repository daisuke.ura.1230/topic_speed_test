#!/usr/bin/env python
import rospy
import sys
from std_msgs.msg import Empty

if __name__ == "__main__":
    rospy.init_node("speed_test")

    pub_rate = rospy.get_param("~test")

    publisher = rospy.Publisher("empty", Empty, queue_size=1)

    rospy.loginfo("Rate : " + str(pub_rate))
    if pub_rate == 0:
        exit()

    rate = rospy.Rate(pub_rate)

    for i in range(5)[::-1]:
        rospy.sleep(1.0)
        print(i)

    for i in range(pub_rate * 10):
        publisher.publish(Empty())
        rate.sleep()

    rospy.loginfo("done")

